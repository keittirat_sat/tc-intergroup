<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if (!empty($_SERVER['HTTP_HOST'])) {
	$http_host = $_SERVER['HTTP_HOST'];
	$site_url = 'https://' . $http_host;
	define('WP_HOME', $site_url);
	define('WP_SITEURL', $site_url);
	define('WP_CONTENT_URL', $site_url . '/wp-content');
	define('WP_PLUGIN_URL', $site_url . '/wp-content/plugins');
}

define('DB_NAME', 'trycatch_intergroup');
define('FS_METHOD', 'direct');
/** MySQL database username */
define('DB_USER', 'trycatch');

/** MySQL database password */
define('DB_PASSWORD', 'gdiupo500510xxx');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@PG7O5`.>|nfa2U2`^/,Ic;|TZn}a_7u{;LfR|D+p;HYJcX~pF-+oiU0W<UsIvOD');
define('SECURE_AUTH_KEY',  'i4.PtyXqZ|HbXL<AI,CX)^yuxzu-C[/@yqR[WQ*5_d-1Y=_txv _EL$E#XC.|C! ');
define('LOGGED_IN_KEY',    'BqKp]XvB^<l{Zo~+xMuK-$Ani.!n<W1X,*VgUmD{OxqYKWF`ax&]Ex=can|-A(<B');
define('NONCE_KEY',        'un/h*G9]kJSPx]*qTfUVncB]@]<I ~$H1sclMls]d4ox U84@s^@pR*xWE9f~1h8');
define('AUTH_SALT',        'bpui,k&egE2^2A_%hH8$:{DK^b/j#Gs#6Tfc#0l<<kz:vFb82$Ys0IIlm3m#()bG');
define('SECURE_AUTH_SALT', '5:#vwuCVi58O0h+30]Lhe&Se EL?8Qw^ILKvu-Q)(5<|]`&a[bCkS4o+?nn@|DTi');
define('LOGGED_IN_SALT',   '[C57Tk[Fn|z?1<~3z,f+sDu$oONlW;db^mMzhi^V>oMndN(E uYNcO)}-8vB^BCU');
define('NONCE_SALT',       'U G%7PYE^C/ t-/t)3CC(R|E!Js,xdF]VY[Y|7i|t7hib6#v{C-jnOe8rq.lz7Z$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'int_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
