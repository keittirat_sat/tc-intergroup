<?php

get_header();
wp_reset_postdata();

echo '<div class="other_top_page">';

switch ($cat) {
    case 3:
    case 9:
        include 'general_news.php';
        break;
	 case 8:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
		include 'realestate_news.php';
		break;
    case 2:
    case 12:
    case 13:
    case 20:
    case 21:
    case 22:
    case 23:
        include 'project.php';
        break;
    default:
        echo "<h1 class='txt_center white'>Not found {$cat}</h1>";
        break;
}

echo '</div>'; 

get_footer();
