<!--Footer-->
<div class="footer_upper">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 txt_center hidden-xs">
                <img src="<?php bloginfo('template_directory'); ?>/img/footer logo.png" class="img-responsive">
            </div>
            <div class="col-sm-6 white">
                <div style="padding: 25px 0px;">
                    <?php $post = get_post(47); ?>
                    <?php setup_postdata($post); ?>
                    <h4><?php the_title(); ?></h4>
                    <?php the_excerpt() ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
            <div class="col-sm-4 white">
                <div style="padding: 25px 0px;">
                    <h4>Address</h4>
                    <div class="row">
                        <label class="col-xs-3">Tel&nbsp;&amp;&nbsp;Fax</label>
                        <div class="col-xs-9">053-142363</div>
                    </div>
                    <div class="row">
                        <label class="col-xs-3">Phone</label>
                        <div class="col-xs-9">091-0679551</div>
                    </div>
                    <div class="row">
                        <label class="col-xs-3">Email</label>
                        <div class="col-xs-9">i.intergroup2014@gmail.com</div>
                    </div>
                    <div class="row">
                        <label class="col-xs-3">Address</label>
                        <div class="col-xs-9">44/67 ศุภาลัย มอนเต้บิซ ตำบล หนองป่าครั่ง อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer_bottom hidden-xs">
    <?php $menu = get_plain_menu(); ?>
    <ul class="footer_menu">
        <?php foreach ($menu as $each_link) : ?>
            <li><a href="<?php echo $each_link['url'] ?>"><?php echo $each_link['title'] ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<!--Footer-->

<script type="text/javascript">
    $(function() {
        $('.contact_form').ajaxForm({
            beforeSend: function() {
                $('#send_btn').button("loading");
            },
            complete: function(res) {
                var json = $.parseJSON(res.responseText);
                if (json.status === "success") {
                    $('.contact_alert').removeClass('hide');
                    $('.contact_form').trigger("reset");
                } else {
                    alert('Cannot process your request');
                }
                $('#send_btn').button("reset");
            }
        });

        $(".more_btn_hover, .map_banner").mouseenter(function() {
            $(this).find('img').stop().animate({
                opacity: 1
            });
        }).mouseleave(function() {
            $(this).find('img').stop().animate({
                opacity: 0
            });
        });
    });
</script>

<?php wp_footer() ?>
</body>

</html>