<div class="container m_margin_top_min50">
    <div class="row">
        <div class="col-xs-12">
            <img src="<?php bloginfo('template_directory'); ?>/img/menu_newsandjobs.png" class="img-responsive">
        </div>
    </div>
    <div class="row" style="padding: 20px 0px;">
        <div class="col-xs-12">
            <p>
                <a href="<?php echo home_url() ?>" class="gray">หน้าแรก</a>
                <span class="white"> // </span>
                <a href="<?php echo get_page_link(49) ?>" class="gray">ข่าวทั้งหมด</a>
                <span class="white"> // </span>
                <span class="gray"><?php echo get_cat_name($cat) ?></span>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 content_archieve">
            <?php if (have_posts()): ?>
                <?php while (have_posts()): the_post(); ?>
                    <div class="row" style="padding-top: 30px;">
                        <div class="col-xs-2">
                            <?php $img_id = get_post_thumbnail_id(); ?>
                            <?php if ($img_id): ?>
                                <?php $img = get_all_size_image($img_id) ?>
                                <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                            <?php else: ?>
                                <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-10 white">
                            <h3 class="quark" style="margin: 0px;">
                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h3>
                            <?php the_excerpt() ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php else: ?>
                <h2 class="txt_center quark white">ไม่มีข่าวในระบบ</h2>
            <?php endif; ?>
        </div>
        <div class="col-sm-4">
            <?php include "sidebar_general_news.php"; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
//    $(function() {
//        $('#menu-item-50').addClass("current-menu-item");
//    });
</script>