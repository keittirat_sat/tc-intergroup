<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.png" type="image/x-icon" />
    <title>
        <?php global $page, $paged; ?>
        <?php wp_title('|', true, 'right'); ?>
        <?php bloginfo('name'); ?>
        <?php $site_description = get_bloginfo('description', 'display'); ?>
        <?php if ($site_description && (is_home() || is_front_page())) echo " | $site_description"; ?>
        <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
    </title>

    <!--CSS Section-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

    <!--JS Section-->
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.7.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
    <!-- <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4UzDRbjkkxOl9Hzgden0RCDtUJCwLOVE" type="text/javascript"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.map.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.form.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.foggy.min.js"></script>
    <script src="https://use.fontawesome.com/6a60e8fe62.js"></script>

    <?php if (is_singular() && get_option('thread_comments')) : ?>
        <?php wp_enqueue_script('comment-reply'); ?>
    <?php endif; ?>

    <?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>

    <!--Main Menu-->
    <div class="main_menu hidden-xs">
        <div class="container">

            <div class="row">
                <div class="col-xs-2">
                    <a href="<?php home_url() ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/img/header_logo.png" alt="InterGroup">
                    </a>
                </div>
                <div class="col-xs-10">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-main-menu-container txt_center  font_20 hidden-xs hidden-sm', 'menu_class' => 'menu clearfix')); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="mobile-nav hidden-sm hidden-md hidden-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                    <a href="<?php home_url() ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/img/header_logo.png" alt="InterGroup" class="img-responsive">
                    </a>
                </div>
                <div class="col-xs-8">
                    <?php $plain_menu = get_plain_menu(); ?>
                    <div class="pull-right" style=" padding:15px 20px 0 0;">
                        <div class="dropdown hidden-lg hidden-md hidden-sm" style="padding: 5px 0px 0px 5px;">
                            <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                                <?php foreach ($plain_menu as $each_res_menu) : ?>
                                    <li>
                                        <a href="<?php echo $each_res_menu['url']; ?>"><?php echo $each_res_menu['title']; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Main Menu-->