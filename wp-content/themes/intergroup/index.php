<?php if (!empty($_POST)): ?>
    <?php echo cms_mail($_POST['name'], $_POST['email'], "Website Contact - ", $_POST['detail'], $to = "keittirat@gmail.com") ?>
<?php else: ?>

    <?php get_header(); ?>

    <!--Section Propotext-->
    <div class="propotext hidden-xs">
        <img src="<?php bloginfo('template_directory'); ?>/img/main_text.png" style="padding: 137px 0px;">
    </div>
    <div class="hidden-sm hidden-md hidden-lg">
        <img src="<?php bloginfo('template_directory'); ?>/img/main_text.png" style="padding: 40px 0px;" class="img-responsive">
    </div>
    <!--/Section Propotext-->

    <!--Slide Index-->
    <div class="slide_index hidden-xs">
        <div class="hRound"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-8" style="position: relative;">
                    <?php $post_slide = get_posts(array("category" => 5)); ?>
                    <?php foreach ($post_slide as $index => $post): setup_postdata($post); ?>
                        <div class="frame_slide" id="<?php echo "slide_{$index}"; ?>" data-order="<?php echo $index ?>">
                            <h1 class="quark txt_center"><?php the_title() ?></h1>
                            <?php the_content() ?>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>

                    <!--Bullet-->
                    <ul class="slide_bullet">
                        <?php for ($bIndex = 0; $bIndex < count($post_slide); $bIndex++): ?>
                            <li class="bullet" id="<?php echo "bullet_{$bIndex}"; ?>"><?php echo $bIndex ?></li>
                        <?php endfor; ?>
                    </ul>
                    <!--/Bullet-->
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $(window).scroll(function () {
                var offsetTop = $('body').scrollTop();
                offsetTop = (offsetTop * 0.75);
                var init = -904 + offsetTop;
                if (init >= -904) {
                    //console.log(init);

                    $('.slide_index').stop().animate({'margin': '0px'},
                    {
                        step: function (now, fx) {
                            $(fx.elem).css("background-position", "0px " + init + "px");
                        },
                        duration: 400
                    }
                    );
                }
            });

            var total = $('.frame_slide').length;
            var current = 0;

            $('.frame_slide').hide().css({opacity: '0', "z-index": 0});

            $('.frame_slide').first().show().animate({opacity: 1}, function () {
                $(this).css({"z-index": "3"});
                current = $(this).attr("data-order");
                $('.bullet').removeClass("active");
                $("#bullet_" + current).addClass("active");
            });

            setInterval(function () {
                var next_target = "#slide_" + current;

                $(next_target).animate({opacity: 0}, 400, function () {

                    $('.bullet').removeClass("active");

                    $(this).css({"z-index": 0}).hide();

                    current = (current + 1) % total;

                    $("#bullet_" + current).addClass("active");

                    $('#slide_' + current).show();
                    $('#slide_' + current).animate({"opacity": 1}, 400, function () {
                        $(this).css({"z-index": 3});
                    });

                });
            }, 3000);

            $('.partner_list').mouseenter(function () {
                $(this).find('.opt_zoom').stop().animate({'background-size': '110%'});
            }).mouseleave(function () {
                $(this).find('.opt_zoom').stop().animate({'background-size': '100%'});
            });
        });
    </script>
    <!--/Slide Index-->

    <!--Body-->
    <div class="body_content">
        <div class="container" style="padding: 55px auto">
            <div class="row white" style="padding-bottom: 20px;">
                <div class="col-xs-12">
                    <?php $post            = get_post(22); ?>
                    <?php setup_postdata($post) ?>
                    <?php the_content() ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>

            <?php $partner_list    = get_posts(array("category" => 7, 'numberposts' => 15)); ?>
            <?php $rows            = 0; ?>
            <?php $post_obj[$rows] = array(); ?>


            <?php foreach ($partner_list as $index => $post): setup_postdata($post) ?>
                <?php $order           = get_field("order"); ?>
                <?php $company[$order] = $post; ?>
                <?php wp_reset_postdata(); ?>
            <?php endforeach; ?>

            <?php ksort($company) ?>

            <?php $index = 0; ?>
            <?php foreach ($company as $each_post): ?>
                <?php array_push($post_obj[$rows], $each_post) ?>
                <?php if ($index % 3 == 2): ?>
                    <?php $rows++ ?>
                    <?php $post_obj[$rows] = array(); ?>
                <?php endif; ?>
                <?php $index++; ?>
            <?php endforeach; ?>

            <?php foreach ($post_obj as $each_rows): ?>
                <div class="row txt_center" style="padding-bottom: 20px;">
                    <?php foreach ($each_rows as $i => $post): setup_postdata($post); ?>
                        <?php if (count($each_rows) < 3): ?>
                            <?php $add_style = "style='float: none; display: inline-block;'"; ?>
                        <?php endif; ?>
                        <div class="col-sm-4 partner_list" <?php echo $add_style; ?>>
                            <?php $img = get_all_size_image(get_post_thumbnail_id()); ?>
                            <div class="hidden-xs">
                                <div class='opt_zoom' style="background-image: url('<?php echo $img['large']; ?>');">
                                    <img src="<?php bloginfo('template_directory'); ?>/img/partner_mask.png" class="img-responsive" >
                                </div>
                                <h1 class="quark orange txt_center">
                                    <a href="<?php echo get_page_link(6) ?>#<?php echo get_the_ID(); ?>" class="orange"><?php the_title() ?></a>
                                </h1>
                            </div>

                            <div class="hidden-sm hidden-md hidden-lg">
                                <a href="<?php the_permalink(); ?>" class='opt_zoom' style="background-image: url('<?php echo $img['large']; ?>'); display: block;">
                                    <img src="<?php bloginfo('template_directory'); ?>/img/partner_mask.png" class="img-responsive" >
                                </a>
                            </div>
                            <?php the_excerpt(); ?>
                            <hr class="hidden-sm hidden-md hidden-lg" />
                        </div>

                        <?php wp_reset_postdata(); ?>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

            <div class="row">
                <div class="col-sm-8 news_blog">
                    <div class="row">
                        <div class="col-xs-12 news_blog"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 news_area">
                            <?php $news_list = get_posts(array("category" => '3,9', 'numberposts' => 5)); ?>
                            <?php if (count($news_list)): ?>
                                <?php foreach ($news_list as $post): setup_postdata($post) ?>
                                    <div class="row" style="padding: 10px 0px;">
                                        <div class="col-sm-2 col-xs-4">
                                            <?php $img_id = get_post_thumbnail_id(); ?>
                                            <a class="orange2" href="<?php the_permalink() ?>">
                                                <?php if ($img_id): ?>
                                                    <?php $img = get_all_size_image($img_id) ?>
                                                    <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                                                <?php else: ?>
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="col-sm-10 col-xs-8">
                                            <h3 class="quark" style="margin: 0px;">
                                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                            </h3>
                                            <div class="ellipsis"><?php the_excerpt() ?></div>
                                        </div>
                                    </div>
                                    <?php wp_reset_postdata(); ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <h2 class="txt_center quark">ไม่มีข่าวในระบบ</h2>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 news_blog_bottom txt_center">
                            <a href="<?php echo get_page_link(49) ?>" class="more_btn_hover">
                                <img src="<?php bloginfo('template_directory'); ?>/img/news_readmore_btn_hover.png" >
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 hidden-xs">
                    <div class="row">
                        <div class="col-xs-12 contact_blog orange quark txt_center">
                            <h1>มีคำถาม</h1>
                            <h1 class="margin_top_0">หรือข้อเสนอแนะ</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-success hide contact_alert" role="alert">
                                <strong>สำเร็จ</strong> ระบบได้ทำการจัดส่งข้อความเรียบร้อยแล้ว
                            </div>
                            <form class="form contact_form" method="post" action="<?php echo site_url(); ?>">
                                <div class="form-group">
                                    <input type="text" placeholder="Name" required="" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="email" placeholder="Email" required="" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <textarea placeholder="Detail" required="" name="detail" class="form-control" style="resize: none;" rows="12"></textarea>
                                </div>
                                <div class="form-group txt_right">
                                    <button class="btn btn-warning" type="submit" data-loading-text="Sending..." id="send_btn">SUBMIT</button>
                                    <button class="btn btn-default" type="reset">RESET</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <a href="<?php echo get_page_link(4); ?>" class="map_banner">
                                <img src="<?php bloginfo('template_directory'); ?>/img/main_map_banner_hover.png" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!--Properties-->
            <div class='row'>
                <div class='col-xs-12 txt_center white'>
                    <p><img class="img-responsive" src='<?php bloginfo('template_directory'); ?>/img/main_sell_text.png'  style='padding: 40px 0px 20px;'></p>
                    <p>ด้วยประสบการณ์การทำงานด้านอสังหาริมทรัพย์มานาน เราจึงพร้อมด้วยคำแนะนำต่างๆ ที่สามารถดูแลคุณ<br>ไม่ว่าจะเป็นการฝากซื้อ-ฝากขาย อสังหาฯ ทุกรูปแบบ หรือต้องการคำปรึกษาเราพร้อมให้คำแนะนำทุกด้าน</p>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-2 Prop Prop_L hidden-xs'></div>
                <div class='col-sm-8 prop_area'>
                    <?php $news_list = get_posts(array("category" => 8, 'numberposts' => 5)); ?>
                    <?php if (count($news_list)): ?>
                        <?php foreach ($news_list as $post): setup_postdata($post) ?>
                            <div class="row" style="padding: 10px 0px;">
                                <div class="col-sm-2 col-xs-4">
                                    <?php $img_id = get_post_thumbnail_id(); ?>
                                    <?php if ($img_id): ?>
                                        <?php $img = get_all_size_image($img_id) ?>
                                        <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                                    <?php else: ?>
                                        <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                                    <?php endif; ?>
                                </div>
                                <div class="col-sm-10 col-xs-8">
                                    <h3 class="quark ellipsis" style="margin: 0px;">
                                        <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <div class="ellipsis"><?php the_excerpt() ?></div>
                                </div>
                            </div>
                            <?php wp_reset_postdata(); ?>
                        <?php endforeach; ?>
                        <div class="row">
                            <div class="col-xs-12 txt_right">
                                <a href="<?php echo get_category_link(8) ?>" class="btn btn-warning">Read more</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <h2 class="txt_center quark">ไม่มีข่าวในระบบ</h2>
                    <?php endif; ?>
                </div>
                <div class='col-xs-2 Prop Prop_R hidden-xs'></div>
            </div><!--Properties-->

        </div>
    </div>
    <!--Body-->

    <?php get_footer(); ?>

<?php endif; ?>