<?php get_header(); ?>
<div class="other_top_page">
    <div class="container m_margin_top_min50">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-xs-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/menu_contactus.png" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-12 pull-right">
                <div class="row">
                    <div class="col-xs-12 orange quark txt_center">
                        <h1>มีคำถาม</h1>
                        <h1 class="margin_top_0">หรือข้อเสนอแนะ</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-success hide contact_alert" role="alert">
                            <strong>สำเร็จ</strong> ระบบได้ทำการจัดส่งข้อความเรียบร้อยแล้ว
                        </div>
                        <form role="form" class="form contact_form" method="post" action="<?php echo site_url() ?>">
                            <div class="form-group">
                                <input type="text" placeholder="Name" required="" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Email" required="" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea placeholder="Detail" required="" name="detail" class="form-control" style="resize: none;" rows="12"></textarea>
                            </div>
                            <div class="form-group txt_right">
                                <button class="btn btn-warning" type="submit" data-loading-text="Sending..." id="send_btn">SUBMIT</button>
                                <button class="btn btn-default" type="reset">RESET</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-xs-12 quark white font_18 pull-left">
                <div id="map_canvas"></div>
                <h1 class="orange">INTERGROUP</h1>
                <p style="margin-bottom: 0px;">44/67 ศุภาลัย มอนเต้บิซ ตำบล หนองป่าครั่ง อำเภอเมืองเชียงใหม่ จังหวัดเชียงใหม่ 50000</p>
                <p>TEL&nbsp;&amp;&nbsp;FAX: 053-142363</p>
                <p>PHONE: 091-0679551</p>
                <p>EMAIL: i.intergroup2014@gmail.com</p>
                <p>
                    <a href="http://www.facebook.com/cpmchiangmai" class="orange" style="text-transform: uppercase">
                        <img src="<?php bloginfo('template_directory'); ?>/img/contact_fb_icon.png">
                        http://www.facebook.com/cpmchiangmai
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#map_canvas').gmap({
            'mapTypeId': google.maps.MapTypeId.HYBRID,
            'zoom': 17,
            'center': '18.8059297,98.9579616'
        }).bind('init', function(ev, map) {
            $('#map_canvas').gmap('addMarker', {
                'position': '18.8059297,98.9579616'
            });
        });
    });
</script>
<?php get_footer(); ?>