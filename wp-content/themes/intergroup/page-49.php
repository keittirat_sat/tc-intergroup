<?php get_header(); ?>
<div class="other_top_page">
    <div class="container m_margin_top_min50">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/menu_newsandjobs.png" class="img-responsive">
            </div>
        </div>
        <div class="row" style="padding: 20px 0px;">
            <div class="col-xs-12">
                <p>
                    <a href="<?php echo home_url() ?>" class="gray">หน้าแรก</a>
                    <span class="white"> // </span>
                    <a href="<?php echo home_url() ?>" class="gray">ข่าวทั้งหมด</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 content_archieve">
                <?php wp_reset_postdata() ?>
                <?php $news_list = get_posts(array("category" => '3,8,9', 'numberposts' => 10)); ?>
                <?php if (count($news_list)): ?>
                    <?php foreach ($news_list as $post): setup_postdata($post) ?>
                        <div class="row" style="padding-top: 30px;">
                            <div class="col-sm-2 col-xs-4">
                                <a class="orange2" href="<?php the_permalink() ?>">
                                    <?php $img_id = get_post_thumbnail_id(); ?>
                                    <?php if ($img_id): ?>
                                        <?php $img = get_all_size_image($img_id) ?>
                                        <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                                    <?php else: ?>
                                        <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="col-sm-10 col-xs-8 white">
                                <h3 class="quark ellipsis" style="margin: 0px;">
                                    <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="ellipsis"><?php the_excerpt() ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata() ?>
                <?php else: ?>
                    <h2 class="txt_center quark white">ไม่มีข่าวในระบบ</h2>
                <?php endif; ?>
            </div>
            <div class="col-sm-4">
                <div class="row" style="padding-bottom: 30px;">
                    <div class="col-xs-12">
                        <h3 class="orange quark txt_center bold">Category ประเภทของข่าว</h3>
                        <ul class="sidebar_menu custom_bullet">
                            <?php $sMenu = get_plain_menu("secondary"); ?>
                            <?php foreach ($sMenu as $side_menu): ?>
                                <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo $side_menu["url"] ?>" class="white"><?php echo $side_menu["title"] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="<?php echo get_page_link(4); ?>" class="map_banner">
                            <img src="<?php bloginfo('template_directory'); ?>/img/main_map_banner_hover.png" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>