<?php get_header(); ?>
<?php wp_reset_postdata() ?>

<?php $partner_list = get_posts(array("category" => 7, 'numberposts' => 15)); ?>

<?php foreach ($partner_list as $post): setup_postdata($post) ?>
    <?php $order           = get_field("order"); ?>
    <?php $company[$order] = $post; ?>
    <?php wp_reset_postdata(); ?>
<?php endforeach; ?>

<?php ksort($company) ?>

<div class="other_top_page_about">
    <div class="container m_margin_top_min50">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-xs-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/menu_aboutus.png" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 white">
                <?php the_content() ?>
            </div>
        </div>

        <div class="hidden-sm hidden-md hidden-lg">
            <h2 class="quark orange">รายละเอียดบริษัทในเครือ</h2>
            <ul class="white">
                <?php foreach ($company as $post): setup_postdata($post); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

    </div>

    <div class="orange_ribbon hidden-xs">
        <div class="container">
            <?php foreach ($company as $post): setup_postdata($post); ?>
                <a class="col-xs-1 txt_center btn_for_partner_about" href="#<?php the_ID(); ?>" id="<?php the_ID() ?>_trigger" data-post_id="<?php the_ID() ?>" style="width: 14.28% !important;">
                    <?php the_title() ?>
                    <img src="<?php bloginfo('template_directory'); ?>/img/about_point.png">
                </a>
                <?php wp_reset_postdata() ?>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="container hidden-xs" style="margin-top: 30px;">
        <?php foreach ($company as $post): setup_postdata($post); ?>
            <div class="content_info" id="partner_<?php echo the_ID() ?>">
                <div class="row" style="padding-bottom: 30px;">
                    <div class="col-xs-8">
                        <?php $post_id = get_the_ID(); ?>
                        <?php switch ($post_id): case 39: ?>
                                <?php $link = "about_logo_idc.png"; ?>
                                <?php break; ?>
                            <?php case 36: ?>
                                <?php $link = "about_logo_imd.png"; ?>
                                <?php break; ?>
                            <?php case 33: ?>
                                <?php $link = "about_logo_ifm.png"; ?>
                                <?php break; ?>
                            <?php case 30: ?>
                                <?php $link = "about_logo_idev.png"; ?>
                                <?php break; ?>
                            <?php case 27: ?>
                                <?php $link = "about_logo_sm.png"; ?>
                                <?php break; ?>
                            <?php case 24: ?>
                                <?php $link = "about_logo_anec.png"; ?>
                                <?php break; ?>
                            <?php case 441: ?>
                                <?php $link = "about_logo_cpm.png"; ?>
                                <?php break; ?>
                        <?php endswitch; ?>
                        <img src="<?php bloginfo('template_directory'); ?>/img/<?php echo $link ?>" class="img-responsive">
                    </div>

                    <?php $cate = get_field("portfolio_category_id"); ?>

                    <?php if ($cate != 0): ?>
                        <div class="col-xs-4 txt_center">
                            <a href="<?php echo get_category_link($cate) ?>" class="go_to_project"></a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="content_partner">
                            <?php the_content() ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php wp_reset_postdata() ?>
        <?php endforeach; ?>
    </div>
</div>

<script>
    $(function () {
        $('.btn_for_partner_about').click(function () {
            var id = $(this).attr('data-post_id');
            console.log(id);

            $('.btn_for_partner_about').removeClass('active');
            $(this).addClass('active');

            $('.content_info').hide(0, function () {
                $('#partner_' + id).show(0);
            });
        });
        if (location.hash == "") {
            $('.btn_for_partner_about').first().trigger("click");
        } else {
            x = location.hash;
            $(x + "_trigger").trigger("click");
        }
    });
</script>

<?php get_footer(); ?>