<?php get_header(); ?>
<div class="other_top_page">
    <div class="container m_margin_top_min50">
        <div class="row">
            <div class="col-xs-12"><img src="<?php bloginfo('template_directory'); ?>/img/menu_project.png"></div>
        </div>
        <div class="row" style="padding: 20px 0px;">
            <div class="col-xs-12">
                <p>
                    <a href="<?php echo home_url() ?>" class="gray">หน้าแรก</a>
                    <span class="white"> // </span>
                    <a href="<?php echo get_page_link(60) ?>" class="gray">Project</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 content_archieve">
                <?php wp_reset_postdata() ?>
                <?php $news_list = get_posts(array("category" => '2,12,13', 'numberposts' => 10)); ?>
                <?php if (count($news_list)) : ?>
                    <?php foreach ($news_list as $post) : setup_postdata($post) ?>
                        <div class="row" style="padding-top: 30px;">
                            <div class="col-xs-4 col-sm-2">
                                <?php $img_id = get_post_thumbnail_id(); ?>
                                <a class="orange2" href="<?php the_permalink() ?>">
                                    <?php if ($img_id) : ?>
                                        <?php $img = get_all_size_image($img_id) ?>
                                        <img src="<?php echo ($img["thumbnail"]); ?>" class="img-responsive">
                                    <?php else : ?>
                                        <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="col-sm-10 col-xs-8 white">
                                <h3 class="quark ellipsis" style="margin: 0px;">
                                    <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                </h3>
                                <div class="ellipsis"><?php the_excerpt() ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata() ?>
                <?php else : ?>
                    <h2 class="txt_center quark white">ไม่มีข่าวในระบบ</h2>
                <?php endif; ?>
            </div>
            <div class="col-sm-4">
                <h3 class="orange quark txt_center bold">Project Navigation</h3>

                <!--IDC-->
                <h4 class="siteLabel"><a href="<?php echo get_category_link(12) ?>" class="white">IDC</a></h4>
                <?php $site_list = get_posts(array("category" => '12', 'numberposts' => 5)); ?>
                <?php if (count($site_list)) : ?>
                    <ul class="custom_bullet">
                        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
                            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
                            <?php wp_reset_postdata() ?>
                        <?php endforeach; ?>
                    </ul>

                <?php else : ?>
                    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
                <?php endif; ?>
                <!--/IDC-->

                <!--Anec-->
                <h4 class="siteLabel"><a href="<?php echo get_category_link(2) ?>" class="white">ANEC</a></h4>
                <?php $site_list = get_posts(array("category" => '2', 'numberposts' => 5)); ?>
                <?php if (count($site_list)) : ?>
                    <ul class="custom_bullet">
                        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
                            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
                            <?php wp_reset_postdata() ?>
                        <?php endforeach; ?>
                    </ul>

                <?php else : ?>
                    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
                <?php endif; ?>
                <!--/Anec-->

                <!--I-Dev-->
                <h4 class="siteLabel"><a href="<?php echo get_category_link(13) ?>" class="white">I-Dev</a></h4>
                <?php $site_list = get_posts(array("category" => '13', 'numberposts' => 5)); ?>
                <?php if (count($site_list)) : ?>
                    <ul class="custom_bullet">
                        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
                            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
                            <?php wp_reset_postdata() ?>
                        <?php endforeach; ?>
                    </ul>
                <?php else : ?>
                    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
                <?php endif; ?>
                <!--/I-Dev-->

                <!--CPM-->
                <h4 class="siteLabel"><a href="<?php echo get_category_link(22) ?>" class="white">CPM</a></h4>
                <?php $site_list = get_posts(array("category" => '22', 'numberposts' => 5)); ?>
                <?php if (count($site_list)) : ?>
                    <ul class="custom_bullet">
                        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
                            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
                            <?php wp_reset_postdata() ?>
                        <?php endforeach; ?>
                    </ul>
                <?php else : ?>
                    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
                <?php endif; ?>
                <!--/CPM-->

                <div class="row" style="padding-top: 30px;">
                    <div class="col-xs-12">
                        <a href="<?php echo get_page_link(4); ?>" class="map_banner">
                            <img src="<?php bloginfo('template_directory'); ?>/img/main_map_banner_hover.png" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>