<?php get_header() ?>
<?php wp_reset_postdata(); ?>
<div class="container m_margin_top_min50">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="quark orange bold"><?php the_title() ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8 content_archieve white">
            <?php the_content(); ?>
        </div>
        <div class="col-xs-4">
            <?php include "sidebar_general_news.php"; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>