<div class="row" style="padding-bottom: 30px;">
    <div class="col-xs-12">
        <h3 class="orange quark txt_center bold">Category ประเภทของข่าว</h3>
        <ul class="sidebar_menu">
            <?php $sMenu = get_plain_menu("secondary"); ?>
            <?php foreach ($sMenu as $side_menu): ?>
                <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo $side_menu["url"] ?>" class="white"><?php echo $side_menu["title"] ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<div class="row" style="padding-bottom: 30px;">
    <div class="col-xs-12">
        <h3 class="orange quark txt_center bold">พื้นที่อสังหาริมทรัพย์</h3>
        <ul class="sidebar_menu">
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(14) ?>" class="white"><?php echo get_cat_name(14) ?></a></li>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(15) ?>" class="white"><?php echo get_cat_name(15) ?></a></li>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(16) ?>" class="white"><?php echo get_cat_name(16) ?></a></li>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(17) ?>" class="white"><?php echo get_cat_name(17) ?></a></li>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(18) ?>" class="white"><?php echo get_cat_name(18) ?></a></li>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php echo get_category_link(19) ?>" class="white"><?php echo get_cat_name(19) ?></a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <a href="<?php echo get_page_link(4); ?>" class="map_banner">
            <img src="<?php bloginfo('template_directory'); ?>/img/main_map_banner_hover.png" class="img-responsive">
        </a>
    </div>
</div>