<h3 class="orange quark txt_center bold">Project Navigation</h3>

<!--IDC-->
<h4 class="siteLabel"><a href="<?php echo get_category_link(12) ?>" class="white">IDC</a></h4>
<?php $site_list = get_posts(array("category" => '12', 'numberposts' => 5)); ?>
<?php if (count($site_list)) : ?>
    <ul class="custom_bullet">
        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata() ?>
<?php else : ?>
    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
<?php endif; ?>
<!--/IDC-->

<!--Anec-->
<h4 class="siteLabel"><a href="<?php echo get_category_link(2) ?>" class="white">ANEC</a></h4>
<?php $site_list = get_posts(array("category" => '2', 'numberposts' => 5)); ?>
<?php if (count($site_list)) : ?>
    <ul class="custom_bullet">
        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata() ?>
<?php else : ?>
    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
<?php endif; ?>
<!--/Anec-->

<!--I-Dev-->
<h4 class="siteLabel"><a href="<?php echo get_category_link(13) ?>" class="white">I-Dev</a></h4>
<?php $site_list = get_posts(array("category" => '13', 'numberposts' => 5)); ?>
<?php if (count($site_list)) : ?>
    <ul class="custom_bullet">
        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata() ?>
<?php else : ?>
    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
<?php endif; ?>
<!--/I-Dev-->

<!--CPM-->
<h4 class="siteLabel"><a href="<?php echo get_category_link(22) ?>" class="white">CPM</a></h4>
<?php $site_list = get_posts(array("category" => '22', 'numberposts' => 5)); ?>
<?php if (count($site_list)) : ?>
    <ul class="custom_bullet">
        <?php foreach ($site_list as $post) : setup_postdata($post); ?>
            <li><i class="orange">&raquo;</i>&nbsp;<a href="<?php the_permalink() ?>" class="white"><?php the_title() ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata() ?>
<?php else : ?>
    <h3 class="white txt_center quark">ไม่พบรายการใดๆ ในส่วนนี้</h3>
<?php endif; ?>
<!--/CPM-->

<div class="row" style="padding-top: 30px;">
    <div class="col-xs-12">
        <a href="<?php echo get_page_link(4); ?>" class="map_banner">
            <img src="<?php bloginfo('template_directory'); ?>/img/main_map_banner_hover.png" class="img-responsive">
        </a>
    </div>
</div>