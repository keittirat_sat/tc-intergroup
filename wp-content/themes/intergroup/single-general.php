<?php get_header(); ?>
<?php wp_reset_postdata() ?>
<div class="other_top_page">
    <div class="container m_margin_top_min50">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/menu_project.png" class="img-responsive">
            </div>
        </div>
        <div class="row" style="padding: 20px 0px;">
            <div class="col-xs-12">
                <p>
                    <a href="<?php echo home_url() ?>" class="gray">หน้าแรก</a>
                    <span class="white"> // </span>
                    <a href="<?php echo get_page_link(60) ?>" class="gray">Project</a>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 content_archieve">
                <div class="row" style="padding-top: 30px;">
                    <div class="hidden-xs">
                        <div class="col-xs-2">
                            <?php $img_id = get_post_thumbnail_id(); ?>
                            <?php if ($img_id): ?>
                                <?php $img = get_all_size_image($img_id) ?>
                                <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                            <?php else: ?>
                                <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-10 white">
                            <h2 class="quark" style="margin: 0px; font-weight: bold;">
                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h2>
                        </div>
                    </div>

                    <!--display in responsive mode-->
                    <div class="hidden-lg hidden-md hidden-sm">
                        <div class="col-xs-12">
                            <h2 class="quark" style="margin: 0px; font-weight: bold;">
                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h2>
                            <?php $img_id = get_post_thumbnail_id(); ?>
                            <?php if ($img_id): ?>
                                <?php $img = get_all_size_image($img_id) ?>
                                <p>
                                    <img src="<?php echo($img["medium"]); ?>" class="img-responsive thumbnail">
                                </p>

                            <?php else: ?>
                                <p>
                                    <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive thumbnail">
                                </p>

                            <?php endif; ?>
                        </div>
                    </div><!-- /display in responsive mode-->
                </div>

                <div class="row"
                     style="padding-top: 15px;">
                    <div class="col-xs-12 white">
                        <?php the_content() ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">

                <?php $cate_id = the_category_ID(false); ?>

                <?php switch ($cate_id): case 2: ?>
                    <?php case 12: ?>
                    <?php case 13: ?>
                        <?php include "sidebar_project_list.php"; ?>
                        <?php break; ?>
                    <?php default: ?>
                        <?php include "sidebar_general_news.php"; ?>
                        <?php break; ?>
                <?php endswitch; ?>

            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.colorbox').colorbox({'maxWidth': '90%', 'maxHeight': '90%'});
    });
</script>

<?php get_footer(); ?>