<?php get_header(); ?>
<?php wp_reset_postdata() ?>
<div class="other_top_page">
    <div class="container m_margin_top_min50">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php bloginfo('template_directory'); ?>/img/menu_realestatenews.png" class="img-responsive">
            </div>
        </div>
        <div class="row" style="padding: 20px 0px;">
            <div class="col-xs-12">
                <p>
                    <a href="<?php echo home_url() ?>" class="gray">หน้าแรก</a>
                    <span class="white"> // </span>
                    <a href="<?php echo get_category_link(8) ?>" class="gray">Real Estate</a>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8 content_archieve">
                <div class="row" style="padding-top: 30px;">
                    <!--display in normal mode-->
                    <div class="hidden-xs">
                        <div class="col-xs-2">
                            <?php $img_id = get_post_thumbnail_id(); ?>
                            <?php if ($img_id): ?>
                                <?php $img = get_all_size_image($img_id) ?>
                                <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive">
                            <?php else: ?>
                                <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-10 white">
                            <h2 class="quark" style="margin: 0px; font-weight: bold;">
                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h2>
                        </div>
                    </div><!-- /display in normal mode-->

                    <!--display in responsive mode-->
                    <div class="hidden-lg hidden-md hidden-sm">
                        <div class="col-xs-12">
                            <h2 class="quark" style="margin: 0px; font-weight: bold;">
                                <a class="orange2" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h2>
                            <?php $img_id = get_post_thumbnail_id(); ?>
                            <?php if ($img_id): ?>
                                <?php $img = get_all_size_image($img_id) ?>
                                <p>
                                    <img src="<?php echo($img["thumbnail"]); ?>" class="img-responsive thumbnail">
                                </p>

                            <?php else: ?>
                                <p>
                                    <img src="<?php bloginfo('template_directory'); ?>/img/screenshot.png" class="img-responsive">
                                </p>

                            <?php endif; ?>
                        </div>
                    </div><!-- /display in responsive mode-->
                </div>

                <!--Relate Image-->
                <div class="row" style="padding-top: 15px;">
                    <?php $all_img = get_all_post_image(get_the_ID()) ?>
                    <?php foreach ($all_img as $img): ?>
                        <div class="col-xs-3">
                            <a href="<?php echo $img['large'] ?>" class="thumbnail thumbnail-image colorbox" title="<?php echo $img['attachment']->post_excerpt ?>" rel="colorbox">
                                <img src="<?php echo $img['thumbnail'] ?>" alt="<?php echo $img['attachment']->post_excerpt ?>">
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!--/Relate Image-->

                <?php $field = get_field("google_map"); ?>
                <?php if ($field): ?>
                    <!--Map-->
                    <div class="row white ">
                        <div class="col-xs-12">
                            <div class="section_properties">
                                <h2 class="quark orange">แผนที่</h2>
                                <div id="map_canvas" style="height: 400px;"></div>
                                <p><?php echo $field['address']; ?></p>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#map_canvas').gmap({
                                            'mapTypeId': google.maps.MapTypeId.HYBRID,
                                            'zoom': 17,
                                            'center': '<?php echo "{$field['lat']},{$field['lng']}"; ?>'
                                        }).bind('init', function (ev, map) {
                                            $('#map_canvas').gmap('addMarker', {'position': '<?php echo "{$field['lat']},{$field['lng']}"; ?>'});
                                        });

                                        $('.colorbox').colorbox({'maxWidth': '90%', 'maxHeight': '90%'});
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!--/Map-->
                <?php endif; ?>

                <!--Content-->
                <div class="row">
                    <div class="col-xs-12 white" style="word-break: break-all">
                        <h2 class="quark orange">รายละเอียด</h2>
                        <?php the_content() ?>
                    </div>
                </div>
                <!--/Content-->
            </div>

            <div class="col-sm-4">

                <?php $cate_id = the_category_ID(false); ?>

                <?php switch ($cate_id): case 2: ?>
                    <?php case 12: ?>
                    <?php case 13: ?>
                        <?php include "sidebar_project_list.php"; ?>
                        <?php break; ?>
                    <?php default: ?>
                        <?php include "sidebar_general_news.php"; ?>
                        <?php break; ?>
                <?php endswitch; ?>

            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>