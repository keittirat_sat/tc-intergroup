<?php

wp_reset_postdata();
$cate_id = the_category_ID(false);

switch ($cate_id) {
    case 8:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
        include "single-properties.php";
        break;
    default:
        include "single-general.php";
        break;
}